/*! \file  parseGPRMC.c
 *
 *  \brief Parse out the $GPRMC message from the GPS
 *
 *  \author jjmcd
 *  \date July 24, 2017, 9:13 AM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include <string.h>
#include "gpsTime.h"


/*! parseGPRMC - Parse out the $GPRMC message from the GPS */

/*! Pulls time, status and date from $GPRMC message. Ignores latitude
 *  and longitude.
 *
 * \param szLine char * - String containint the GPS message
 * \returns none
 */
void parseGPRMC(char *szLine)
{
  char *p, *q;
  int i;

  p = &szLine[7];
  i=0;
  while ( *p != ',' )
    {
      szRMCtime[i] = *p;
      i++; p++; q++;
    }
  szRMCtime[10] = '\0';
  p++; /* Skip comma */
  szRMCstatus[0] = *p;
  szRMCstatus[1] = '\0';
  p+=3; /* Skip over */
  while ( *p != ',' )   /* Skip over latitude */
    {
      p++;;
    }
  p+=3; /* Skip over N/S */
  while ( *p != ',' )   /* Skip over longitude */
    {
      p++;;
    }
  p+=3; /* Skip over E/W */
  while ( *p != ',' )   /* Skip over speed */
    {
      p++;;
    }
  p++; /* Skip comma */
  while ( *p != ',' )   /* Skip over course */
    {
      p++;;
    }
  p++; /* Skip comma */
  i=0;
  while ( *p != ',' )
    {
      szRMCdate[i] = *p;
      i++; p++; q++;
    }
  szRMCdate[10] = '\0';


}
