/*! \file  showSatellites.c
 *
 *  \brief Show the number of satellites used
 *
 *  \author jjmcd
 *  \date August 26, 2017, 8:51 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/TFT.h"
#include "gpsTime.h"


/*! showSatellites - Show the number of satellites used */

/*!
 *
 */
void showSatellites( void )
{
  TFTsetFont((fontdatatype*) DJS);
  TFTsetColorX(SATELLITECOLOR);

  TFTprint(szGGAnumSat, 100, 220, 0);
  TFTprint("satellites", 133, 220, 0);
}
