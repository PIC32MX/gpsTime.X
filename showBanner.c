/*! \file  showBanner.c
 *
 *  \brief Display a startup banner for the clock
 *
 *  \author jjmcd
 *  \date July 28, 2017, 12:04 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/TFT.h"
#include "gpsTime.h"


/*! showBanner - Display a startup banner for the clock */

/*! Displays the compile date and time as well as the git commit hash
 *
 * \param szDate char * - Compile date
 * \param szTime char * - Compile time
 * \returns none
 */
void showBanner(char *szDate, char *szTime)
{
  TFTsetFont((fontdatatype*) BigFont);
  TFTsetColorX(TEXTCOLOR);
  TFTsetBackColorX(BLACK);
  TFTclear();
  TFTprint("GPS Clock",CENTER,80,0);
  TFTsetColorX(COMPILEDCOLOR);
  TFTsetFont((fontdatatype*) DJS);
  TFTprint("Compiled",CENTER,120,0);
  TFTprint(szDate,CENTER,140,0);
  TFTprint(szTime,CENTER,155,0);
  TFTsetColorX(COMMITCOLOR);
  TFTprint(COMMIT,CENTER,220,0);
  delay(3000);
  nBanner = 1;

}
