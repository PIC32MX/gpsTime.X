## GPS based clock

Displays the local time in huge numbers and the UTC time in somewhat
smaller numbers, on a SainSmart TFT. Time is gotten from a GPS module,
preferable an NEO-8 although a NE-06 will work but will not
synchronize as quickly or in difficult curcumstances.

Clock is built on the graphics terminal board. The Gerbers for this
board may be found at https://gitlab.com/PIC32MX/tft-board-pcb

---

![Clock image](images/ZuluClock.jpg)

---

### Connecting the GPS

![Connections](images/GPSconnectionsReverse.png)

---
