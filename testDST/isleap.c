/*! \file  isleap.c
 *
 *  \brief Determine if a year is a leap year
 *
 *
 */


/*! isleap - Determine if a year is a leap year */
/*! isleap() - Determine if a year is a leap year.  Return TRUE or FALSE.
 *
 * \param year int - Year to be tested
 * \returns TRUE if the year is a leap year, else FALSE
 */
int isleap( int year )
{
return ( (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0) );
}

