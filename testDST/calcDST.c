/*! \file  calcDST.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date July 24, 2017, 2:00 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include "gpsTime.h"

int daywk(int, int, int);

/*! calcDST - */

/*!
 *
 */
void calcDST(void)
{
    int nYear;
    int nMonth;
    int nDay;
    int nDateOfSunday;
    int nDayOfMonth;
  //  long lTime;
  int nGMThour;

  nMonth = (szRMCdate[2]-'0')*10+(szRMCdate[3]-'0');

  nDST = 0;
  /* January, February never DST */
  if ( nMonth<3 )
    return;
  /* December never DST */
  if ( nMonth>11 )
    return;
  /* April through October always DST */
  if ( (nMonth>3) && (nMonth<11) )
    {
      nDST = 1;
      return;
    }

  /* Now we need to get ugly */
  nDay   = (szRMCdate[0]-'0')*10+(szRMCdate[1]-'0');
  nYear  = (szRMCdate[4]-'0')*10+(szRMCdate[5]-'0')+2000;

  /* Now handle March */
  if ( nMonth==3 )
    {
      if ( nDay<8 )      /* Second Sunday can't come before the 8th */
        return;
      if ( nDay>14 )     /* Second Sunday can't come after the 14th */
        {
          nDST = 1;
          return;
        }

      for ( nDayOfMonth=8; nDayOfMonth<15; nDayOfMonth++ )
        if ( daywk(nYear,3,nDayOfMonth)==0 )
          nDateOfSunday=nDayOfMonth;

      if (nDay>nDateOfSunday)
        {
          nDST=1;
          return;
        }
      if (nDay<nDateOfSunday)
        return;

      nGMThour = (szRMCtime[0]&0x0f)*10 + (szRMCtime[1]&0x0f);
      
      if ( nGMThour > 6 ) /* GMT-5 = EST */
        {
          nDST = 1;
          return;
        }

      return;
    }
  /* Now for November */
  if ( nMonth==11 )
      {
	  if ( nDay>7 ) /* First Sunday can't be later than the 7th */
	      return;
	  

      for ( nDayOfMonth=1; nDayOfMonth<8; nDayOfMonth++ )
        if ( daywk(nYear,11,nDayOfMonth)==0 )
          nDateOfSunday=nDayOfMonth;

      if (nDay<nDateOfSunday)
        {
          nDST=1;
          return;
        }
      if (nDay>nDateOfSunday)
        return;


      nGMThour = (szRMCtime[0]&0x0f)*10 + (szRMCtime[1]&0x0f);
      
      if ( nGMThour < 6 ) /* GMT-5 = EST */
        {
          nDST = 1;
          return;
        }

      return;
      }
}
