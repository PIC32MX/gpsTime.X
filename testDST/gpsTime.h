
#ifndef EXTERN
#define EXTERN extern
#endif

EXTERN char szRMCtime[32];
EXTERN char szRMCdate[32];
EXTERN int nDST;
