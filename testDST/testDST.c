#include <stdio.h>
#include <string.h>
#define EXTERN
#include "gpsTime.h"

int calcDST(void);
int daywk(int, int, int);

int main()
{
    int dow;

    dow=daywk(2017,7,24);
    printf("Day of week=%d.\n",dow);
    
    strcpy(szRMCtime,"120000");
    strcpy(szRMCdate,"210117");
    calcDST();
    printf("210117 12000 = %d\n",nDST);

    strcpy(szRMCtime,"120000");
    strcpy(szRMCdate,"210717");
    calcDST();
    printf("210717 12000 = %d\n",nDST);

    strcpy(szRMCtime,"065800");
    strcpy(szRMCdate,"120317");
    calcDST();
    printf("120317 065800 = %d\n",nDST);

    strcpy(szRMCtime,"070000");
    strcpy(szRMCdate,"120317");
    calcDST();
    printf("120317 070000 = %d\n",nDST);

    return 0;
}
