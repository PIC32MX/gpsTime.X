/*! \file  simpleTerminal.h
 *
 *  \brief Shared definitions for simple terminal
 *
 *  \author jjmcd
 *  \date March 10, 2014, 12:02 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */

#ifndef SIMPLETERMINAL_H
#define	SIMPLETERMINAL_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef EXTERN
#define EXTERN extern
#endif

/*! Latest commit */
#define COMMIT "6179679971cf1080242e8cb13bcc58a4dfbc7da6"

/*! Color in which to display normal characters */
#define TEXTCOLOR YELLOW

/* Color of datum at the left */
#define DATUMCOLOR DARKSALMON
/* Color of GGA string display */
#define GGACOLOR GREEN3
/* Color of RMC string display with status 'A' (valid) */
#define RMCCOLOR BLUE3
/* Color of RMC string display with sataus 'V' */
#define RMCVCOLOR MAGENTA
/* Color of RMC string display with checksum error */
#define RMCECOLOR PINK
/* Color of RMC string display when nothing good happening*/
#define RMCDCOLOR GOLDENROD
/* Color of RTC time display */
#define RTCCCOLOR CYAN

/*! Width of a displayed character */
#define XDELTA1 10
/* For big font */
#define XDELTA2 13
/*! Height of a displayed character */
#define YDELTA 14

#define BUSYLED _LATB5
#define BUFSIZE 1024

#define BACKGROUND_COLOR BLACK
#define FOREGROUND_COLOR TURQUOISE
#define COLON_COLOR RED
#define COLON_OTHER_COLOR PINK
#define AMCOLOR GREEN4
#define PMCOLOR GREENYELLOW
#define EDTESTCOLOR CYAN3
#define SECONDSCOLOR SANDYBROWN
#define SATELLITECOLOR LIGHTGOLDENROD
#define COMPILEDCOLOR GOLD
#define DATEWARNINGCOLOR PINK
#define DATECOLOR GOLDENROD
#define COMMITCOLOR ORANGE
#define ZULUCOLOR AQUAMARINE
#define POS_1 -5
#define POS_2 60
#define POS_3 154
#define POS_4 239
#define COLON_LEFT 145
#define AMPM_Y 140
#define ZULUY 165
#define ZULUX 50

#define TOP 40
#define BOTTOM 140
#define MIDDLE ((TOP+BOTTOM)/2)

/*! Horizontal position of the next character on the screen */
EXTERN int nScreenX;
/*! Vertical position of the next character on the screen */
EXTERN int nScreenY;
/*! Buffer for incoming characters */
EXTERN char szBuffer[BUFSIZE];
/*! Position to place the next character in the buffer */
EXTERN char *pNextIn;
/*! Position in the buffer of the next character to process */
EXTERN char *pNextOut;
/*! Pointer to the end of the buffer */
EXTERN char *pEndBuffer;
/*! Is the banner still on the screen */
EXTERN int nBanner;
/*! Do we have a date yet */
EXTERN int nDateDirty;
/*! Previous day of week */
EXTERN int nOldDoW;
/*! Current day of week */
EXTERN int nDoW;
/*! Current day of month */
EXTERN int nDoM;
/*! Text name of days of week */
extern char szDays[7][4];

EXTERN char szGGAtime[32];
EXTERN char szGGAlat[32];
EXTERN char szGGAlon[32];
EXTERN char szGGAfix[16];
EXTERN char szGGAnumSat[16];
EXTERN char szRMCtime[32];
EXTERN char szRMCstatus[16];
EXTERN char szRMClat[32];
EXTERN char szRMClon[32];
EXTERN char szRMCdate[32];
EXTERN char szRMCmode[16];
EXTERN int nDST;
EXTERN int nAMPM;

void parseGPGGA(char *);
void parseGPRMC(char *);
void showBanner(char *, char *);
void showDigit(int,int);
void showUTCtime(void);
void showSatellites(void);
void showLocalTime(char *,char *);

#ifdef	__cplusplus
}
#endif

#endif	/* SIMPLETERMINAL_H */

