/*! \file  showLocalTime.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date August 26, 2017, 8:54 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include <string.h>
#include "../include/TFT.h"
#include "gpsTime.h"


/*! showLocalTime - */

/*!
 *
 */
void showLocalTime(char *szLine, char *szOldLine)
{
  char szWork[16];
  int nHour;

  szWork[0] = szLine[7];
  szWork[1] = szLine[8];
  szWork[2] = '\0';
  sscanf(szWork, "%d", &nHour);

  if (nDST)
    nHour -= 4;
  else
    nHour -= 5;
  if (nHour < 0)
    nHour += 24;
  if (nHour > 12)
    {
      nHour -= 12;
      nAMPM = 1;
    }
  else
    nAMPM = 0;
  sprintf(szWork, "%02d", nHour);

  if (nBanner) /* Erase banner if it is still hanging around */
    {
      nBanner = 0;
      TFTclear();
    }

  TFTsetColorX(FOREGROUND_COLOR);
  if (szWork[0] != szOldLine[0])
    {
      if (szWork[0] == '1')
        showDigit(1, POS_1);
      else
        showDigit(11, POS_1);
    }
  if (szWork[1] != szOldLine[1])
    showDigit(szWork[1]&0x0f, POS_2);
  if (szLine[9] != szOldLine[9])
    showDigit(szLine[9]&0x0f, POS_3);
  if (szLine[10] != szOldLine[10])
    showDigit(szLine[10]&0x0f, POS_4);

  memcpy(szOldLine, szLine, 14);
  szOldLine[0] = szWork[0];
  szOldLine[1] = szWork[1];

  if (szLine[12] % 2)
    TFTsetColorX(COLON_COLOR);
  else
    TFTsetColorX(COLON_OTHER_COLOR);
  TFTfillRect(COLON_LEFT, TOP, COLON_LEFT + 4, TOP + 4);
  TFTfillRect(COLON_LEFT, BOTTOM - 40, COLON_LEFT + 4, BOTTOM - 44);

  /* If date unknown, can't know whether daylight savings */
  TFTsetFont((fontdatatype*) BigFont);
  if (nDateDirty)
    {
      TFTsetColorX(DATEWARNINGCOLOR);
      TFTprint("NO DATE", 10, AMPM_Y, 0);
    }

  /* Show if AM or PM */
  TFTsetColorX(AMCOLOR);
  if (nAMPM)
    {
      TFTsetColorX(PMCOLOR);
      TFTprint("PM", 280, AMPM_Y, 0);
    }
  else
    TFTprint("AM", 280, AMPM_Y, 0);
  TFTsetColorX(EDTESTCOLOR);
  if (nDST)
    TFTprint("EDT", 230, AMPM_Y, 0);
  else
    TFTprint("EST", 230, AMPM_Y, 0);

}
