/*! \file  showUTCtime.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date August 26, 2017, 8:50 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/TFT.h"
#include "gpsTime.h"

/*! showUTCtime - */

/*!
 *
 */
void showUTCtime(void)
{
  TFTsetFont((fontdatatype*) BigFont);
  TFTsetColorX(TURQUOISE);
  if (szRMCtime[5] & 0x01)
    TFTsetColorX(GREENYELLOW);
  TFTprintChar(':', ZULUX + 71, ZULUY + 16);
  TFTprintChar(':', ZULUX + 152, ZULUY + 16);
  TFTsetFont((fontdatatype*) SevenSegNumFont);
  TFTsetColorX(ZULUCOLOR);
  TFTprintChar(szRMCtime[0], ZULUX + 0, ZULUY);
  TFTprintChar(szRMCtime[1], ZULUX + 32, ZULUY);
  TFTprintChar(szRMCtime[2], ZULUX + 84, ZULUY);
  TFTprintChar(szRMCtime[3], ZULUX + 116, ZULUY);
  TFTprintChar(szRMCtime[4], ZULUX + 168, ZULUY);
  TFTprintChar(szRMCtime[5], ZULUX + 200, ZULUY);
}
