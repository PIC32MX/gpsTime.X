/*! \file  setupUART.c
 *
 *  \brief Initialize the UART
 *
 *  Sets the UART interrupt priority to 4, then maps UART2 to
 *  RPB8 and RPB9.  Disables most fancy features of the UART
 *  including CTS/RTS, sets the baud rate, then turns on the UART.
 *
 *  \author jjmcd
 *  \date March 10, 2014, 12:47 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */
#include <xc.h>

/*! setupUART - Initialize the UART */
/*! Sets the UART interrupt priority to 4, then maps UART2 to
 *  RPB8 and RPB9.  Disables most fancy features of the UART
 *  including CTS/RTS, sets the baud rate, then turns on the UART.
 *
 */
void setupUART(void)
{
    /* Set UART interrupt priority and sub-priority */
    IPC9bits.U2IP = 4;       /* UART 2 interrupt priority to 4 */
    IPC9bits.U2IS = 2;       /* UART 2 interupt sub-priority */

    /* Map U2RX to RPB8 */
    //PPSInput(2,U2RX,RPB8);
    U2RXRbits.U2RXR = 4;
    /* Map U2TX to RPB9 */
    //PPSOutput(4,RPB9,U2TX);
    RPB9Rbits.RPB9R = 2;

    /* Configure the UART */
    U2MODEbits.ON     = 0; /* UART off */
    U2MODEbits.SIDL   = 0; /* Continue in idle mode */
    U2MODEbits.IREN   = 0; /* IrDA disabled */
    U2MODEbits.RTSMD  = 1; /* U2RTS is in simplex mode */
    U2MODEbits.UEN    = 0; /* U2TX/U2RX used, U2CTS/U2RTS port controlled */
    U2MODEbits.WAKE   = 1; /* Wake on start bit enabled */
    U2MODEbits.LPBACK = 0; /* Loopback disabled */
    U2MODEbits.ABAUD  = 0; /* Autobaud disabled */
    U2MODEbits.RXINV  = 0; /* U2RX idle state is '1' */
    U2MODEbits.BRGH   = 0; /* Standard speed mode */
    U2MODEbits.PDSEL  = 0; /* 8-bit data no parity */
    U2MODEbits.STSEL  = 0; /* 1 stop bits */

    /* Baud rate */
    U2BRG = 324;  /* 9600 */
    //U2BRG = 1300; /* 2400 */

    /* Take care of status bits */
    U2STAbits.UTXEN   = 1;   /* Enable transmit */
    U2STAbits.URXEN   = 1;   /* Enable rcv */
    U2STAbits.URXISEL = 0;   /* Interrupt on at least 1 character */
    U2STAbits.OERR    = 0;   /* Clear overrun bit */

    /* Now we can turn the UART on */
    U2MODEbits.ON     = 1; /* UART on */
}
