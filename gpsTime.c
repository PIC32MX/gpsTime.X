/*! \file  gpsTime.c
 *
 *  \brief Clock using GPS for time
 *
 *  Application receives messages from NEO6 GPS and displays
 *  Eastern time in large numbers.
 *
 *  \author jjmcd
 *  \date July 22, 2017 7:19 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 */

#include <xc.h>
#include <sys/attribs.h>
#include <string.h>
#include <stdio.h>
#include "../include/ConfigBits.h"
#include "../include/TFT.h"
#include "../include/colors.h"
#define EXTERN
#include "gpsTime.h"

/*! main - GPS clock */

/*! Application reads GPS and displays time
 */
int main(void)
{
  char szLine[256], szOldLine[16];
  int nPos;
  char *p;
  int nHour;
  char szWork[16];
  int nBuffChars;

  /* Initialize UART2 */
  setupUART();

  /* LEDs */
  _TRISB5 = 0;
  BUSYLED = 0;

  /* Initialize the TFT and clear */
  SetupHardware();
  TFTinit(LANDSCAPE);
  TFTsetBackColorX(BLACK);
  TFTsetColorX(TEXTCOLOR);
  TFTclear();
  showBanner(__DATE__,__TIME__);

  /* Select the font and colors, clear the TFT */
  TFTsetFont((fontdatatype*) DJS);

  /* Remember we don't have a date yet */
  nDateDirty = 1;

  /* Initialize the buffer pointers */
  pNextIn = szBuffer;
  pNextOut = szBuffer;
  pEndBuffer = szBuffer + 1023;

  IEC1bits.U2RXIE = 1; /* Enable receive interrupt */
  INTEnableSystemMultiVectoredInt();

  U2STAbits.OERR = 0; /* Clear overrun bit */

  strcpy(szOldLine, "AAAAAAAAAAAA");

  nPos = 0;
  while(pNextIn==pNextOut)
    ;
  TFTclear();

  while (1)
    {
      /* If the in and out pointers are different, char is available */
      if (pNextOut != pNextIn)
        {
          nBuffChars = pNextIn - pNextOut;
          if (nBuffChars < 0)
            nBuffChars += BUFSIZE;
          //            pNextOut++;
          /* Illuminate LED if unprocessed characters */
          if (nBuffChars > 2)
            BUSYLED = 1;
          else
            BUSYLED = 0;
          if (*pNextOut == '$')
            nPos = 0;
          szLine[nPos] = *pNextOut;
          if (*pNextOut == 0x0d)
            {
              szLine[nPos] = '\0';
              /* Set the text color */
              TFTsetColorX(TEXTCOLOR);
              nScreenY = 10;

              if (!strncmp("RMC", &szLine[3], 3))
                {
                  parseGPRMC(szLine);
                  /* Show UTC time */
                  showUTCtime();
                  calcDST();
                  /* Show local time */
                  showLocalTime(szLine,szOldLine);
                }
              /* Show number of satellites used */
              if (!strncmp("GGA", &szLine[3], 3))
                {
                  parseGPGGA(szLine);
                  showSatellites();
                }
            }
          /* Move to the next character */
          pNextOut++;
          nPos++;
          /* Wrap if at end of buffer */
          if (pNextOut > pEndBuffer)
            pNextOut = szBuffer;
          /* Check for framing error */
          if (U2STAbits.FERR)
            {
              TFTsetColorX(HOTPINK);
              TFTprint("  Framing error ", RIGHT, 10, 0);
              TFTsetColorX(TEXTCOLOR);
              U2STAbits.FERR = 0;
            }
          /* Check for overrun error */
          if (U2STAbits.OERR)
            {
              TFTsetColorX(TOMATO);
              TFTprint("  Overrun error ", RIGHT, 20, 0);
              TFTsetColorX(TEXTCOLOR);
              U2STAbits.OERR = 0;
            }
        }
    }
  return 0;
}
