/*! \file  showDigit.c
 *
 *  \brief Display a digit on the LCD
 *
 *  \author jjmcd
 *  \date July 5, 2017, 3:54 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/TFT.h"

#define TOP 2
#define BOTTOM 132
#define MIDDLE ((TOP+BOTTOM)/2)

#define SEGA { TFTfillRect(xoff+12,TOP,     xoff+68,TOP   +12); }
#define SEGB { TFTfillRect(xoff+68,MIDDLE-6,xoff+79,TOP   +12); }
#define SEGC { TFTfillRect(xoff+68,MIDDLE+6,xoff+79,BOTTOM-12); }
#define SEGD { TFTfillRect(xoff+12,BOTTOM, +xoff+68,BOTTOM-12); }
#define SEGE { TFTfillRect(xoff+1, MIDDLE+6,xoff+12,BOTTOM-12); }
#define SEGF { TFTfillRect(xoff+1, MIDDLE-6,xoff+12,TOP   +12); }
#define SEGG { TFTfillRect(xoff+12,MIDDLE-6,xoff+68,MIDDLE +6); }
#define LMBL { TFTfillRect(xoff+1, MIDDLE-6,xoff+12,MIDDLE +6); }
#define RMBL { TFTfillRect(xoff+68,MIDDLE-6,xoff+79,MIDDLE +6); }
#define LBBL { TFTfillRect(xoff+1, BOTTOM,  xoff+12,BOTTOM-12); }
#define LTBL { TFTfillRect(xoff+1, TOP,     xoff+12,TOP   +12); }
#define RTBL { TFTfillRect(xoff+68,TOP,     xoff+79,TOP   +12); }
#define RBBL { TFTfillRect(xoff+68,BOTTOM,  xoff+79,BOTTOM-12); }


void makeAzero(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  SEGA;
  SEGB;
  SEGC;
  SEGD;
  SEGE;
  SEGF;
  LMBL;
  RMBL;
  TFTfillRect(xoff+68,TOP+6,xoff+74,TOP+12);
  TFTfillRect(xoff+68,BOTTOM-6,xoff+74,BOTTOM-12);
  TFTfillRect(xoff+6,TOP+6,xoff+12,TOP+12);
  TFTfillRect(xoff+6,BOTTOM-6,xoff+12,BOTTOM-12);
}

void makeAone(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  TFTfillRect(xoff+34,TOP,xoff+46,BOTTOM);
  TFTfillRect(xoff+24,BOTTOM-10,xoff+56,BOTTOM);
  TFTfillRect(xoff+25,TOP,xoff+35,TOP+10);
}

void makeAtwo(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  SEGA;
  SEGB;
  SEGG;
  SEGE;
  SEGD;
  LBBL;
  TFTfillRect(xoff+68,TOP+6,xoff+74,TOP+12);
  TFTfillRect(xoff+6,MIDDLE+6,xoff+12,MIDDLE);
  TFTfillRect(xoff+68,MIDDLE-6,xoff+74,MIDDLE);
}

void makeAthree(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  SEGA;
  SEGB;
  SEGC;
  SEGD;
  SEGG;
  TFTfillRect(xoff+68,TOP+6,xoff+74,TOP+12);
  TFTfillRect(xoff+68,BOTTOM-6,xoff+74,BOTTOM-12);
}

void makeAfour(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
//  TFTfillRect(xoff+10,MIDDLE-5,xoff+79,MIDDLE+5);
//  TFTfillRect(xoff+1,MIDDLE-5,xoff+10,TOP);
//  TFTfillRect(xoff+70,TOP,xoff+79,BOTTOM);
  SEGB;
  SEGC;
  SEGF;
  SEGG;
  LTBL;
  RTBL;
  RMBL;
  LMBL;
  RBBL;
}

void makeAfive(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  SEGA;
  SEGF;
  SEGG;
  SEGC;
  SEGD;
  LTBL;
  TFTfillRect(xoff+68,BOTTOM-6,xoff+74,BOTTOM-12);
  TFTfillRect(xoff+68,MIDDLE,xoff+74,MIDDLE+6);
  TFTfillRect(xoff+1,MIDDLE-6,xoff+12,MIDDLE+6);
  TFTfillRect(xoff+1,BOTTOM-12,xoff+12,BOTTOM);
  TFTfillRect(xoff+68,TOP,xoff+74,TOP+12);
}

void makeAsix(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  SEGA;
  SEGF;
  SEGC;
  SEGD;
  SEGE;
  SEGG;
  LMBL;
  TFTfillRect(xoff+68,BOTTOM-6,xoff+74,BOTTOM-12);
  TFTfillRect(xoff+6,TOP+6,xoff+12,TOP+12);
  TFTfillRect(xoff+6,BOTTOM-6,xoff+12,BOTTOM-12);
  TFTfillRect(xoff+68,MIDDLE,xoff+74,MIDDLE+6);
}

void makeAseven(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
//  TFTfillRect(xoff+10,TOP,xoff+79,TOP+10);
//  TFTfillRect(xoff+70,TOP,xoff+79,BOTTOM);
  LTBL;
  SEGA;
  RTBL;
  SEGB;
  RMBL;
  SEGC;
  RBBL;
}

void makeAeight(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  SEGA;
  SEGB;
  SEGC;
  SEGD;
  SEGE;
  SEGF;
  SEGG;
  TFTfillRect(xoff+68,TOP+6,xoff+74,TOP+12);
  TFTfillRect(xoff+68,BOTTOM-6,xoff+74,BOTTOM-12);
  TFTfillRect(xoff+6,TOP+6,xoff+12,TOP+12);
  TFTfillRect(xoff+6,BOTTOM-6,xoff+12,BOTTOM-12);
}

void makeAnine(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
  SEGG;
  SEGF;
  SEGA;
  SEGB;
  SEGC;
  SEGD;
  RMBL;
  TFTfillRect(xoff+68,TOP+6,xoff+74,TOP+12);
  TFTfillRect(xoff+68,BOTTOM-6,xoff+74,BOTTOM-12);
  TFTfillRect(xoff+6,TOP+6,xoff+12,TOP+12);
}

void makeAblank(int xoff)
{
  TFTeraseRect(xoff,TOP,xoff+79,BOTTOM);
}

void showDigit( int digit, int offset )
{
  switch(digit)
    {
      case 0:
        makeAzero(offset);
        break;
      case 1:
        makeAone(offset);
        break;
      case 2:
        makeAtwo(offset);
        break;
      case 3:
        makeAthree(offset);
        break;
      case 4:
        makeAfour(offset);
        break;
      case 5:
        makeAfive(offset);
        break;
      case 6:
        makeAsix(offset);
        break;
      case 7:
        makeAseven(offset);
        break;
      case 8:
        makeAeight(offset);
        break;
      case 9:
        makeAnine(offset);
        break;
      default:
        makeAblank(offset);
        break;
    }
}
