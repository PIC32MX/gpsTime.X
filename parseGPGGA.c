/*! \file  parseGPGGA.c
 *
 *  \brief Parse the GPS $GPGGA message
 *
 *  \author jjmcd
 *  \date July 23, 2017, 12:07 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include <string.h>
#include "gpsTime.h"

/*! parseGPGGA - Parse the GPS $GPGGA message */

/*! Reads the $GPGGA message passed in as a string and extracts
 *   the time, latitude, longitude and number of satellites used.
 *
 * \param szLine char * - String containing the GPS message
 * \returns none
 */
void parseGPGGA(char *szLine)
{
  char *p,*q;
  int i;

  p = &szLine[7];
  memset(szGGAtime,0,sizeof(szGGAtime));
  i = 0;
  while ( *p != ',' )
    {
      szGGAtime[i] = *p;
      i++;
      p++;
      q++;
    }
  szGGAtime[10]='\0';
  p++; // get past comma

  memset(szGGAlat,0,sizeof(szGGAlat));
  i = 0;
  while ( *p != ',' )
    {
      szGGAlat[i] = *p;
      i++;
      p++;
      q++;
    }
  szGGAlat[10]='\0';
  p+=3; // get past N and 2 commas

  memset(szGGAlon,0,sizeof(szGGAlon));
  i = 0;
  while ( *p != ',' )
    {
      szGGAlon[i] = *p;
      i++;
      p++;
      q++;
    }
  szGGAlon[10]='\0';
  p+=3; // get past W and 2 commas

  szGGAfix[0]=*p;
  szGGAfix[1]='\0';
  p+=2;  // Get past fix and a comma

  memset(szGGAnumSat,0,sizeof(szGGAnumSat));
  i = 0;
  while ( *p != ',' )
    {
      szGGAnumSat[i] = *p;
      i++;
      p++;
      q++;
    }
  szGGAnumSat[10]='\0';
  p++; // get past commas

}
